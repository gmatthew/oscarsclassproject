@extends('layouts.app') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"><h2>Vote For Best Picture</h2></div>

				<div class="panel-body">
					<div class="col-md-12">
						{!! Form::open(array('method' => 'POST', 'url' =>
						action('HomeController@vote'))) !!} @foreach ($movies as $movie)

						<div class="thumbnail">
							<img src="{{ $movie->image_url }}" alt="{{ $movie->name }}">
							<div class="caption">
								<h4><?php echo Form::radio('movie_id', $movie->id ); ?> {{ $movie->name }}</h4>
							</div>
						</div>
				     @endforeach 
                   </div>

				  

				</div>
				
			</div>
			 <center><button type="submit" class="btn btn-primary">Vote!</button></center>
  {!! Form::close() !!} 
		</div>
	</div>
</div>
@endsection
