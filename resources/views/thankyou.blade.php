@extends('layouts.app') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<center>
				<h1>Thank You!</h1>
				<p>
					You voted <b>{{ $movie->name }}</b> for <b>Best Picture</b>
				</p>
			</center>
		</div>
	</div>
</div>
@endsection
