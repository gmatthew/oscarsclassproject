@extends('layouts.app') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			
			<div class="panel panel-default">
				<div class="panel-heading"><h2>Results For Best Picture</h2></div>
				<table class="table">
					<tr>
						<th>Movie</th>
						<th>Votes</th>
					</tr>

					@foreach ($results as $result)
					<tr>
						<td><img src="{{ $result['movie']->image_url }}"
							alt="{{ $result['movie']->name }}" class="img-thumbnail"
							width=140>
							<h4>{{ $result['movie']->name }}</h4></td>
						<td>{{ $result['count'] }}</td> @endforeach
				
				</table>
			</div>
		</div>

	</div>
</div>
</div>
@endsection
