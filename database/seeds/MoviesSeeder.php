<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Movie;

class MoviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$movies = ['The Big Short', 'Bridge of Spies', 'Brooklyn', 'Mad Max: Fury Road', 'The Martian', 'The Revenant', 'Room', 'Spotlight'];
    	$movie_photos = ['MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_SX300.jpg',
    			'MV5BMjIxOTI0MjU5NV5BMl5BanBnXkFtZTgwNzM4OTk4NTE@._V1_SX300.jpg',
    			'MV5BMzE4MDk5NzEyOV5BMl5BanBnXkFtZTgwNDM4NDA3NjE@._V1_SX300.jpg',
    			'MV5BMTUyMTE0ODcxNF5BMl5BanBnXkFtZTgwODE4NDQzNTE@._V1_SX300.jpg',
    			'MV5BMTc2MTQ3MDA1Nl5BMl5BanBnXkFtZTgwODA3OTI4NjE@._V1_SX300.jpg',
    			'MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_SX300.jpg',
    			'MV5BMjE4NzgzNzEwMl5BMl5BanBnXkFtZTgwMTMzMDE0NjE@._V1_SX300.jpg',
    			'MV5BMjIyOTM5OTIzNV5BMl5BanBnXkFtZTgwMDkzODE2NjE@._V1_SX300.jpg'
    	];
    	
    	for($i=0; $i < count($movies); $i++) {
    		$movie = $movies[$i];
    		$image_url = $movie_photos[$i];
    		
    		$movieObj = new \App\Movie();
    		$movieObj->name = $movie;
    		$movieObj->image_url = $image_url;
    		$movieObj->save();
    	}
    	
        
    }
}
