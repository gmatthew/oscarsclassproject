<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('movies', function($table)
    	{
    		$table->engine = 'InnoDB';
    	
    		$table->increments('id');
    		$table->string('name');
    		$table->string('image_url', 255);
    		$table->timestamps();
    	});
    	
    	Schema::create('votes', function($table)
    	{
    		$table->engine = 'InnoDB';
    	
    		$table->increments('id');
    		$table->integer('user_id')->unsigned();
    		$table->integer('movie_id')->unsigned();
    		$table->unique('user_id');
    		$table->timestamps();
 
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop('movies');
    	Schema::drop('votes');
    }
}
