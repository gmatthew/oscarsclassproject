<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Movie;
use App\Vote;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	
  		$movies = \App\Movie::all();
    	return view('home', ['movies' => $movies] );
    }
    
    public function vote(Request $request) {
    	
    	$user = \Auth::user();
    	$movie_id = $request->input('movie_id');
    	$movie = \App\Movie::find($movie_id);
    	
    	if ($movie_id) {
    		$vote = \App\Vote::firstOrNew(['user_id' => $user->id ]);
    		$vote->user_id = $user->id;
    		$vote->movie_id = $movie_id;
    		$vote->save();
    		
    		
    	}
    	
    	return view('thankyou', [ 'movie' => $movie ]);
    }
    
    public function results() {
    	
    	$votes = DB::table('votes')
    					->select('movie_id', DB::raw('count(*) as count'))
    					->groupBy('movie_id')
    					->orderBy('count')
    					->get();
    	
    	$data = [];
    	
    	foreach ($votes as $vote) {
    		$data['results'][] = [ 'movie' => \App\Movie::find($vote->movie_id),
    							  'count' => $vote->count
    							];	
    	}
    					
    	return view('results', $data);
    	
    }
}
